#include <iostream>

template <typename ValueType> //template osztalyt hasznalunk hogy barmilyen adattipussal mukodjon
class BinTree{
protected:
	


	class Node{ //csomopont osztajunk
	private:
		ValueType value; //a csomopont erteke
		Node* left;//a csomopont baloldali gyereke
		Node* right;//a csomopont jobboldali gyereke
		int count{0}; //szamlalo, amivel szamon tartjuk hanyszor

		Node(const Node &);//tiltom a masolo, es mozgato szemantikat mivel ezt majd az egesz fa szintjen kezelem
		Node & operator=(const Node &);
		Node(const Node &&);
		Node & operator=(const Node &&);

	public:
		Node(ValueType value): value(value), left(nullptr), right(nullptr) {}//csomopont "konstruktora", erteknek megadja a kapott erteket, a gyerekeit pedig nullptr-ek re allitja
		ValueType getValue(){return value;}//visszater az adott csomopontban talalt ertekkel
		Node * leftChild(){return left;}//visszater az adott csomopont baloldali gyerekere mutato pointerrel
		Node * rightChild(){return right;}//visszater az adott csomopont jobboldali gyerekere mutato pointerrel
		void leftChild(Node * node){left = node;}//tovabb lep az adott csomopont baloldali gyerekere
		void rightChild(Node * node){right = node;}// tovabb lep az adott csomopont jobboldali gyerekere
		int getCount(){return count;}
		void incCount(){++count;}
	};

	Node *root; //a fank gyokerere mutato mutato
	Node *treep; //a bejaraskor a fan a helyzetunket mutato mutato
	int depth{0}; //melyseg szamlalo

	private:

	public:
		BinTree(Node *root = nullptr, Node *treep = nullptr): root(root), treep(treep) //fa konstruktor
		{
			std::cout << "BT ctor" << std::endl;//standard kimenetre kiirom, hogy lefutott a binfa konstruktor
		}
				
		BinTree(const BinTree & old) {
			std::cout << "BT copy ctor" << std::endl;

			root = cp(old.root, old.treep);
		}

		Node * cp(Node *node, Node *treep)//a masolo fuggveny preorder bejarassal jarja vegig a fat
		{
			Node * newNode = nullptr;

			if(node)
			{
				newNode = new Node(node->getValue());

				newNode->leftChild(cp(node->leftChild(), treep));
				newNode->rightChild(cp(node->rightChild(), treep));
				
				if(node == treep)
				{
					this->treep = newNode;
				}
			}

			return newNode;
		}

		BinTree & operator=(const BinTree & old) {
			std::cout << "BT copy assing" << std::endl;

			BinTree tmp{old};
			std::swap(*this, tmp);
			return *this;
		}
		BinTree(BinTree && old)
		{
			std::cout << "BT move ctor" << std::endl;

			root = nullptr;
			*this = std::move(old);
		}
		BinTree & operator=(BinTree && old)
		{
			std::cout << "BT move assing" << std::endl;

			std::swap(old.root, root);
			std::swap(old.treep, treep);
				
			return *this;
		}

		~BinTree(){
			
			std::cout << "BT dtor" << std::endl;
		
			deltree(root);
		}
		BinTree<ValueType> & operator<<(ValueType value);
		void print(){print(root, std::cout);}
		void print(Node *node, std::ostream & os);
		void deltree(Node *node);
};
template <typename ValueType, ValueType vr, ValueType v0>
class ZLWTree : public BinTree<ValueType> {
	public:
		ZLWTree(): BinTree<ValueType>(new typename BinTree<ValueType>::Node(vr)) { //ZLW fa konstruktor
			this->treep = this->root;
		}
		ZLWTree & operator<<(ValueType value);
};

template <typename ValueType>
BinTree<ValueType> & BinTree<ValueType>::operator<<(ValueType value)//ertek "shift"-elo operator implementacioja
{
	if(!treep) {//Ha a fa-pointerunk meg nem mutat sehvoa...

		root = treep = new Node(value);// ...akkor mutasson a gyokerre, es ott letrehozunk egy uj csomopontot a kapott ertekkel.

	} else if (treep->getValue() == value) {//Ha a fa-pointer alatt talat csomopont ertekevel megeggyezik a kapott ertekkel...

		treep -> incCount();// ...akkor noveljuk a csomopont szamlalojat.

	} else if (treep -> getValue() > value) {//Ha a fa-pointer alatt talat csomopont ertekenel kisebb...

		if(!treep->leftChild()){///...es nincs baloldali gyereke...

			treep-> leftChild(new Node(value));//...akkor letrehozok egyet es erteknek megkapja az erteket,...


		} else {//... , ha viszont van baloldali gyereke ...
			treep = treep -> leftChild();//... akkor raalitom a fa-pointert...
			*this << value;//... es bele probalom "shiftelni" az erteket.
		}

	} else if (treep -> getValue() < value) {//Ha a fa-pointer alatt talat csomopont ertekenel nagyobb...
		if(!treep->rightChild()){//...es nincs jobboldali gyereke...

			treep-> rightChild(new Node(value));//...akkor letrehozok egyet es erteknek megkapja az erteket,...


		} else {//... , ha viszont van jobboldali gyereke ...
			treep = treep -> rightChild();//... akkor raalitom a fa-pointert...
			*this << value;//... es bele probalom "shiftelni" az erteket.
		}
	}

	treep = root;//a fa-pointert visszaalitjuk a gyokerre

	return *this;
}

template <typename ValueType, ValueType vr, ValueType v0>
ZLWTree<ValueType, vr, v0> & ZLWTree<ValueType, vr, v0>::operator<<(ValueType value)//ertek "shift"-elo operator implemenacioja a ZLW faba
{

	if(value == v0){//Ha a bejovo ertek "0"...

		if(!this->treep->leftChild()) {//... es nincs baloldali gyereke a csomopontnak

			typename BinTree<ValueType>::Node * node = new typename BinTree<ValueType>::Node(value);
			this->treep->leftChild(node);
			this->treep = this->root;

		} else {
			this->treep = this->treep->leftChild();
		}

	} else {

		if(!this->treep->rightChild()) {

			typename BinTree<ValueType>::Node * node = new typename BinTree<ValueType>::Node(value);
			this->treep->rightChild(node);
			this->treep = this->root;

		} else {
			this->treep = this->treep->rightChild();
		}

	}

	return *this;
}

template <typename ValueType>
void BinTree<ValueType>::print(Node *node, std::ostream & os)//a fa kiiro fuggveny indorder bejarassal jarja be a fat
{
	if(node)
	{
		++depth;
		print(node->leftChild(), os);

		for(int i{0}; i<depth; ++i)
			os << "---";
		os << node->getValue() << " " << depth << " " << node->getCount() << std::endl;

		print(node->rightChild(), os);
		--depth;
	}
}

template <typename ValueType>
void BinTree<ValueType>::deltree(Node *node)//a fa torlo fuggveny, posztorder bejarassal jarja be a fat
{
	if(node)
	{
		deltree(node->leftChild());
		deltree(node->rightChild());

		delete node;
	}
}

int main(int argc, char** argv, char** env)
{
	BinTree<int> bt;

	bt << 8 << 9 << 5 << 2 << 7;

	bt.print();

	std::cout << std::endl;

	ZLWTree<char, '/', '0'> zt;

	zt <<'0'<<'0'<<'0'<<'0'<<'0';

	zt.print();

	ZLWTree<char, '/', '0'> zt2(zt);

	ZLWTree<char, '/', '0'> zt3;

	zt3 << '1' << '1' << '1' << '1' << '1';

	std::cout << "***" << std::endl;

	zt = zt3;

	std::cout << "***" << std::endl;

	ZLWTree<char, '/', '0'> zt4 = std::move(zt2);

	BinTree<std::string> bts;

	bts << "alma" << "korte" << "banan" << "korte";

	bts.print();

}