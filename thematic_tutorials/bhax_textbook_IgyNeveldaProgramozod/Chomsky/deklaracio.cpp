

int main()
{
	int a; //egy egeszet
	int *b = &a; //egy mutatot ami az "a" egesz cimere mutat
	int &r = a; //c++ban egesz referenciaja
	int c[5]; // egeszek tombje
	int (&tr)[5] = c; //c++ban egeszek tombjenek referenciaja
	int *d[5];//egeszekre mutato mutatok tombje
	int *h ();//egeszre mutato mutatot visszaado fuggveny
	int *(*l) ();//egeszre mutato mutatora mutato mutatot visszaterito fuggveny
	int (*v (int c)) (int a, int b); //egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény
	int (*(*z) (int)) (int, int);//függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvényre

	return 0;
}
