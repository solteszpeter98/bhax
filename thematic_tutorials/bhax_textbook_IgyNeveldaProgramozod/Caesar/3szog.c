#include <stdio.h>
#include <stdlib.h>

int
main ()
{
    int nr = 5;//megadjuk a haromszogmatrixunk sorainak szamat
    double **tm;//deklaraljuk a tombunket amiben a double tombokre mutato mutatokat fogjuk tarolni

    if ((tm = (double **) malloc (nr * sizeof (double *))) == NULL)//lefoglaljuk a dounle-okre mutato mutatokat tartalmazo tombhoz szukseges memoriat, ha ez valami miatt nem sikerulbe a program hibaval kilep
    {
        return -1;
    }

    for (int i = 0; i < nr; ++i)
    {
        if ((tm[i] = (double *) malloc ((i + 1) * sizeof (double))) == NULL)//lefoglaljuk a double tombjeinket amikben a haromszog matrix adatait fogjuk tarolni
        {
            return -1;
        }

    }

    for (int i = 0; i < nr; ++i)
        for (int j = 0; j < i + 1; ++j)
            tm[i][j] = i * (i + 1) / 2 + j;//feltoltjuk a tombunket adatokkal

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);//kiiratjuk a tombunkben talalhato adatokat
        printf ("\n");
    }

    tm[3][0] = 42.0;
    *(tm + 3)[1] = 43.0;	// mi van, ha itt hiányzik a külső ()
    *(tm[3] + 2) = 44.0;
    *(*(tm + 3) + 3) = 45.0;

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }

    for (int i = 0; i < nr; ++i)
        free (tm[i]);// felszabaditjuk a double tombokhoz tartozo memoriacimeket

    free (tm);//vegul pedig felszabaditjuk a mutatokat tartalmazo tombhoz tartozo memoriacimet

    return 0;
}
